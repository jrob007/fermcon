import os

import paho.mqtt.client as paho


class Mqtt:

    def __init__(self, broker, port, username, password, clientid, topic):
        self.topic_registrations = {}
        self.topic = topic
        client = paho.Client(clientid)
        client.on_message = self.on_message
        self.client = client
        client.username_pw_set(username=username, password=password)
        client.connect(broker, port)
        self.client.loop_start()

    def start(self):
        self.client.loop_start()

    def stop(self):
        self.client.loop_stop()

    def subscribe(self, topic, callback):
        fulltopic = os.path.join(self.topic, topic)
        self.client.subscribe(fulltopic)
        self.topic_registrations[fulltopic] = callback

    def on_message(self, client, userdata, message):
        value = str(message.payload.decode("utf-8"))
        self.topic_registrations[message.topic](value)

    def publish(self, topic, value, group=""):
        full_topic = os.path.join(self.topic, group, topic)
        self.client.publish(full_topic, value)
