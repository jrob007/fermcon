import json

from apscheduler.schedulers.blocking import BlockingScheduler

from bme280fc import Bme280fc
from controller import Controller
from ds18b20 import Ds18b20


class Core:

    def __init__(self, broker, relay):
        self.broker = broker
        self.relay = relay
        self.controller = Controller(self.broker.publish, self.relay)
        self.scheduler = BlockingScheduler()
        self.scheduler.add_job(self.controller.run, 'cron',
                               second='*/30', id='fermcon')
        self.broker.subscribe("config", self.set_config)

    def exit(self):
        print("Core exiting.")
        self.scheduler.shutdown()
        self.relay.exit()
        self.broker.stop()

    def start(self):
        self.broker.start()
        self.scheduler.start()

    def set_config(self, config_json):
        print("Updating config")
        config = json.loads(config_json)
        print("Clearing sensors")
        self.controller.clear_sensors()
        print("Adding Sensors")
        self.add_ds18b_sensor(config)
        self.add_bme280_sensor(config)
        print("Clearing relays")
        self.relay.clear_relays()
        print("Adding Relays")
        self.add_relays(config)
        print("Clearing Targets")
        self.controller.clear_targets()
        print("Adding Targets")
        self.add_targets(config)
        self.update_schedule(config['schedule'])

    def update_schedule(self, schedule):
        print("Setting schedule: " + schedule)
        self.scheduler.reschedule_job(
            'fermcon', trigger='cron', second=schedule)
        print("schedule updated")

    def add_ds18b_sensor(self, config):
        sensors = config["ds18b20"]
        for sensor in sensors:
            name = sensor["name"]
            id = sensor["id"]
            a = sensor["a"]
            b = sensor["b"]
            sensor = Ds18b20(id, a, b)
            self.controller.add_sensor(name, sensor.read_value, "temperature")

    def add_bme280_sensor(self, config):
        sensors = config["bme280"]
        for sensor in sensors:
            temperature = sensor["name_temperature"]
            humidity = sensor["name_humidity"]
            pressure = sensor["name_pressure"]
            port = sensor["smbus_port"]
            address = sensor["address"]
            bme = Bme280fc(port, address)
            self.controller.add_sensor(
                temperature, bme.read_temperature, "temperature")
            self.controller.add_sensor(pressure, bme.read_pressure, "pressure")
            self.controller.add_sensor(humidity, bme.read_humidity, "humidity")

    def add_relays(self, config):
        relays = config["relays"]
        for relay in relays:
            name = relay["name"]
            pin = relay["pin"]
            target = relay["target"]
            action = relay["action"]
            self.relay.add_relay(name, pin, target, action)

    def add_targets(self, config):
        targets = config["targets"]
        for target in targets:
            name = target["name"]
            sensor = target["sensor"]
            value = target["value"]
            hi_tolerance = target.get("hi_tolerance", 0)
            lo_tolerance = target.get("lo_tolerance", 0)
            self.controller.add_target(
                name, sensor, value, hi_tolerance, lo_tolerance)
