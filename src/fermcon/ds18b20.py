class Ds18b20:

    base_dir = '/sys/bus/w1/devices/'

    def __init__(self, id, calibration_a, calibration_b):
        self.id = id
        self.calibration_a = calibration_a
        self.calibration_b = calibration_b

    def read_temp_raw(self):
        device_file = self.base_dir + '/' + self.id + '/w1_slave'
        f = open(device_file, 'r')
        lines = f.readlines()
        f.close()
        return lines

    def read_value(self):
        lines = self.read_temp_raw()
        while lines[0].strip()[-3:] != 'YES':
            time.sleep(0.2)
            lines = read_temp_raw()
        equals_pos = lines[1].find('t=')
        if equals_pos != -1:
            temp_string = lines[1][equals_pos+2:]
            temp_c = float(temp_string) / 1000.0
            temp_c_calib = round(
                temp_c * self.calibration_a + self.calibration_b, 2)
            return temp_c_calib
