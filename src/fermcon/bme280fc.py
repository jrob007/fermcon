import time

import bme280
import smbus2


class Bme280fc:

    def __init__(self, smbus_port, address):
        self.smbus_port = int(smbus_port)
        self.address = int(address, 16)
        bus = smbus2.SMBus(self.smbus_port)
        self.calibration_params = bme280.load_calibration_params(
            bus, self.address)
        bus.close()
        self.last_check = time.perf_counter() - 0.25

    def read_temperature(self):
        return round(self.read_values().temperature, 2)

    def read_humidity(self):
        return round(self.read_values().humidity, 2)

    def read_pressure(self):
        return round(self.read_values().pressure, 2)

    def read_values(self):
        if time.perf_counter() - self.last_check < 0.25:
            return self.data
        bus = smbus2.SMBus(self.smbus_port)
        self.data = bme280.sample(
            bus, self.address, self.calibration_params)
        bus.close()
        self.last_check = time.perf_counter()
        return self.data
