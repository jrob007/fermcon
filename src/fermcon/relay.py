from dataclasses import dataclass

import RPi.GPIO as GPIO


@dataclass
class Device:
    name: str
    pin: int
    state: str
    target: str
    action: str


class Relay:

    disable_flag = 1
    disable_text = 0
    enable_flag = 0
    enable_text = 1

    def __init__(self, publish):
        self.devices = []
        self.publish = publish
        GPIO.setmode(GPIO.BCM)

    def add_relay(self, name, pin, target, action):
        print(f"Adding relay: {name} on pin {pin} to {action} {target}")
        self.devices.append(
            Device(name, pin, self.disable_text, target, action))
        GPIO.setup(pin, GPIO.OUT, initial=self.disable_flag)

    def enable(self, target, action):
        self.set_by_target_action(
            target, action, self.enable_flag, self.enable_text)

    def disable(self, target, action):
        self.set_by_target_action(
            target, action, self.disable_flag, self.disable_text)

    def is_enabled(self, target, action):
        return self.get_by_target_action(target, action).state == self.enable_text

    def is_disabled(self, target, action):
        return self.get_by_target_action(target, action).state == self.disable_text

    def get_by_target_action(self, target, action):
        device = [x for x in self.devices
                  if x.target == target
                  if x.action == action][0]
        return device

    def set_by_target_action(self, target, action, value, state):
        device = [x for x in self.devices
                  if x.target == target
                  if x.action == action][0]
        self.set_device(device, value, state)

    def set_device(self, device, value, state):
        if device.state == state:
            return
        print(f"Setting {device.name} to {state}")
        GPIO.output(device.pin, value)
        device.state = state
        self.publish(device.name, state, "state")

    def clear_relays(self):
        for device in self.devices:
            self.set_device(device, self.disable_flag, self.disable_text)
        self.devices = []

    def exit(self):
        print("Relay exiting.")
        for device in self.devices:
            self.set_device(device, self.disable_flag, self.disable_text)
        GPIO.cleanup()
