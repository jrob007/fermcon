from typing import Any
from dataclasses import dataclass
name: str


@dataclass
class Sensor:
    name: str
    read_value: Any
    last_value: float
    target: str


@dataclass
class Target:
    name: str
    sensor: str
    value: float
    hi_tolerance: float
    lo_tolerance: float


class Controller:

    def __init__(self, publish, relay):
        self.sensors = []
        self.targets = []
        self.publish = publish
        self.relay = relay

    def add_sensor(self, name, read_value, target):
        print(f"Add sensor: {target} {name}")
        self.sensors.append(Sensor(name, read_value, 0, target))

    def add_target(self, name, sensor, value, hi_tolerance, lo_tolerance):
        print(f"Adding target: {name} at {value}")
        target = Target(name, sensor, value, hi_tolerance, lo_tolerance)
        self.targets.append(target)

    def run(self):
        print("looping...")

        for sensor in self.sensors:
            value = sensor.read_value()
            sensor.last_value = value
            self.publish(sensor.name, value, sensor.target)
            print(f"{sensor.target} {sensor.name} {value}")

        for target in self.targets:
            sensor = [x for x in self.sensors
                      if x.target == target.name
                      if x.name == target.sensor][0]
            last_value = sensor.last_value
            lo_value = target.value - target.lo_tolerance
            hi_value = target.value + target.hi_tolerance
            target_increasing = self.relay.is_enabled(target.name, "increase")
            target_decreasing = self.relay.is_enabled(target.name, "decrease")
            if last_value < lo_value and not target_increasing:
                print(
                    f"{target.name} is below tolance of {lo_value} at {last_value}")
                self.relay.enable(target.name, "increase")
            elif last_value >= target.value and target_increasing:
                print(
                    f"{target.name} has increased to target of {target.value}")
                self.relay.disable(target.name, "increase")
            elif last_value > hi_value and not target_decreasing:
                print(
                    f"{target.name} is above tolance of {hi_value} at {last_value}")
                self.relay.enable(target.name, "decrease")
            elif last_value <= target.value and target_decreasing:
                print(
                    f"{target.name} has decreased to target of {target.value}")
                self.relay.disable(target.name, "decrease")
            elif target_increasing:
                print(f"{target.name} increasing")
            elif target_decreasing:
                print(f"{target.name} decreasing")
            else:
                print(
                    f"{target.name} is within tolearnce range of {lo_value} to {hi_value}")
                self.relay.disable(target.name, "increase")
                self.relay.disable(target.name, "decrease")

        for device in self.relay.devices:
            self.publish(device.name, device.state, "state")
            print(f"{device.name} {device.state}")

    def clear_sensors(self):
        self.sensors = []

    def clear_targets(self):
        self.targets = []
