import os
from signal import signal, SIGINT, SIGTERM
from sys import exit

from _version import __version__
from core import Core
from mqtt import Mqtt
from relay import Relay

core = None


def sigint_handler(signal_received, frame):
    print('\nSIGINT or CTRL-C detected. Exiting gracefully')
    core.exit()
    print("exiting...")
    exit(0)


def read_required_env(name):
    value = os.getenv(name)
    if value is None:
        raise ValueError("Enviroment variable missing: " + name)
    return value


def initialize_mqtt():
    broker = read_required_env("FERMCON_MQTT_BROKER")
    port = os.getenv("FERMCON_MQTT_PORT", 1883)
    username = read_required_env("FERMCON_MQTT_USERNAME")
    password = read_required_env("FERMCON_MQTT_PASSWORD")
    clientid = read_required_env("FERMCON_MQTT_CLIENTID")
    topic = read_required_env("FERMCON_MQTT_TOPIC")
    print(f"broker: {broker}, port: {port}, user: {username}, topic: {topic}")
    broker = Mqtt(broker, port, username, password, clientid, topic)
    return broker


def main():
    signal(SIGINT, sigint_handler)
    signal(SIGTERM, sigint_handler)
    print("Version: " + __version__)
    global core
    broker = initialize_mqtt()
    relay = Relay(broker.publish)
    core = Core(broker, relay)
    core.start()


main()
