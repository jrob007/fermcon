. /etc/skel/.bashrc

PS1="${PS1//\\\$/\\n\\\$}"

alias pipfreezeignore='pip freeze | grep -vFxf requirements_ignore.txt'
